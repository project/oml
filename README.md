Outgoing Mail Logger
===============

This module logs all outgoing emails processed by the Drupal mail_alter hook and provides a filterable report.
Much of this module is inspired by the D7 mail-logger module.

[Issue Tracker](https://www.drupal.org/project/issues/oml?version=8.x)

## Features
* Logs outgoing emails in a separate table.
* Provides a logs report with optional search filters (date, message key, to, and subject).
* Configurable report options: results per page, date format
* Loggable message filters for message-type (module-key) and/or module.
* Optional compression of message body to save space.
* Scheduled purge (in days, requires cron)
* Purge all logs
* Resend email
* Exclude body option

## SwiftMailer Integration
This module does not require SwiftMailer, but works best with it.  It implements a SwiftMailer event-listener to capture
message result status.  If not available, status can not be verified and thus is left null.

## Version Requirements
This will probably work on older version of D8, but it was built on D8.9.
