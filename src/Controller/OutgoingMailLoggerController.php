<?php

namespace Drupal\outgoing_mail_logger\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\outgoing_mail_logger\OutgoingMailLogger;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for outgoing_mail_logger routes.
 */
class OutgoingMailLoggerController extends ControllerBase {

  /** @var OutgoingMailLogger */
  protected $outgoing_mail_logger;

  /** @var FormBuilder $form_builder */
  protected $form_builder;

  /**
   * OutgoingMailLoggerController constructor.
   *
   * @param OutgoingMailLogger $outgoing_mail_logger
   * @param FormBuilder $form_builder
   */
  public function __construct(
    OutgoingMailLogger $outgoing_mail_logger,
    FormBuilder $form_builder
  ) {
    $this->outgoing_mail_logger = $outgoing_mail_logger;
    $this->form_builder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('outgoing_mail_logger.service'),
      $container->get('form_builder'),
    );
  }

  /**
   * Log detail view
   *
   * @param int $id
   *
   * @return array
   */
  public function detail(int $id) {

    $build = [];

    if ($log = $this->outgoing_mail_logger->getLogById($id)) {
      $data = $log->toArray();
      $data['sent'] = $log->getSent($this->outgoing_mail_logger->getConfigDateFormat());
      $data['headers'] = json_decode($data['headers'], TRUE);

      $data['user'] = 'Undefined';
      if ($data['uid']) {
        /** @var \Drupal\Core\Session\AccountInterface $user */
        if ($user = User::load($data['uid'])) {
          $data['user'] = $user->getDisplayName() . " ({$data['uid']})";
        }
      } else {
        $data['user'] = t('Anonymous or Offline process');
      }

      $data['status'] = $data['status'] ? t('Sent') : t('Unknown');

      $data['saving_body'] = $this->outgoing_mail_logger->getConfigShouldSaveBody();

      if ($resend_link = $this->outgoing_mail_logger->getResendLink($log, FALSE)) {
        $build['#attached']['library'] = ['core/drupal.dialog.ajax'];
        $resend_link = $resend_link->toRenderable();
        $resend_link['#attributes']['class'][] = 'button';
      } else {
        $resend_link = '';
      }

      $build['content'] = [
        '#theme' => 'outgoing-mail-log-detail',
        '#log' => $data,
        '#resend_link' => $resend_link,
      ];
    } else {
      $this->messenger()->addError("Log not found.");
    }

    return $build;
  }

  /**
   * Handles resend link request and turns resend form
   *
   * @param int $id
   *
   * @return AjaxResponse
   */
  public function resend(int $id) : AjaxResponse
  {

    $response = new AjaxResponse();

    if ($log = $this->outgoing_mail_logger->getLogById($id)) {

      $reload = (bool) \Drupal::request()->query->get('reload');

      $reason = '';
      if ($this->outgoing_mail_logger->canResend($log, $reason)) {
        // Get the modal form using the form builder.
        $modal_form = $this->form_builder->getForm('Drupal\outgoing_mail_logger\Form\ResendForm', $log, $reload);

        // Add an AJAX command to open a modal dialog with the form as the content.
        $response->addCommand(new OpenModalDialogCommand(t('Resend Message Form'), $modal_form, ['width' => '800']));
      } else {
        $response->addCommand(new MessageCommand(t("This message can not be resent: @reason", ['@reason' => $reason]),NULL, ['type' => 'error']));
      }

    } else {
      $response->addCommand(new MessageCommand(t("Log not found."),NULL, ['type' => 'error']));
    }

    return $response;
  }

}
