<?php
/**
 * Project: hammer9-rebuild
 *
 * @copyright 2021 Mosaic Learning Inc.
 */

namespace Drupal\outgoing_mail_logger\Form;


use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\outgoing_mail_logger\Model\OutgoingMailLog;
use Drupal\outgoing_mail_logger\OutgoingMailLogger;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ResendForm extends \Drupal\Core\Form\FormBase {

  /** @var OutgoingMailLogger */
  protected $outgoing_mail_logger;

  /**
   * OutgoingMailLoggerController constructor.
   *
   * @param Connection $database
   * @param OutgoingMailLogger $outgoing_mail_logger
   * @param EmailValidatorInterface $email_validator
   */
  public function __construct(
    OutgoingMailLogger $outgoing_mail_logger
  ) {
    $this->outgoing_mail_logger = $outgoing_mail_logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('outgoing_mail_logger.service'),
    );
  }

  /**
   * @inheritDoc
   */
  public function getFormId()
  {
    return 'oml_resend_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    if (!($log = $this->getLog($form_state))) {
      throw new \RuntimeException('Missing required context argument.');
    }

    return [
      'disclaimer' => [
        '#type' => 'container',
        'content' => [
          '#markup' => t('Please note the following considerations before resending this message:')
            . '<ul>'
            . '<li>' . t('Message formatting may not exactly match original (should be close).') . '</li>'
            . '<li>' . t('Resent messages will NOT contain any attachments sent with the original message.') . '</li>'
            . '</li>'
        ]
      ],
      'confirm' => [
        '#type' => 'container',
        'content' => [
          '#markup' => '<strong>' . t('Click CONFIRM to proceed.') . '</strong>',
        ],
      ],
      'actions' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['form-actions']],
        'confirm' => [
          '#type' => 'submit',
          '#value' => t('Confirm'),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
              'button--primary',
            ],
          ],
          '#ajax' => [
            'callback' => [$this, 'ajaxSubmit'],
            'event' => 'click',
          ],
        ],
        'cancel' => [
          '#type' => 'submit',
          '#value' => t('Cancel'),
          '#attributes' => [
            'class' => [
              'use-ajax',
              'button',
              'button--secondary',
            ],
          ],
          '#ajax' => [
            'callback' => [$this, 'ajaxCancel'],
            'event' => 'click',
          ],
        ]
      ]
    ];
  }

  /**
   * Returns form OutgoingMailLog argument from build-info
   *
   * @param FormStateInterface $form_state
   *
   * @return OutgoingMailLog|null
   */
  protected function getLog(FormStateInterface $form_state) : ?OutgoingMailLog
  {
    $build_info = $form_state->getBuildInfo();
    if (isset($build_info['args'][0]) && $build_info['args'][0] instanceof OutgoingMailLog) {
      return $build_info['args'][0];
    }
  }

  /**
   * Returns reload form argument (build-info[1])
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  protected function shouldReload(FormStateInterface $form_state) : bool
  {
    $build_info = $form_state->getBuildInfo();
    if (isset($build_info['args'][1])) {
      return (bool) $build_info['args'][1];
    }
    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // TODO: Implement submitForm() method.
  }

  public function ajaxSubmit(array &$form, FormStateInterface $form_state)
  {

    $response = new AjaxResponse();

    try {

      if (!($log = $this->getLog($form_state))) {
        throw new \RuntimeException('Missing required context argument.');
      }

      $should_reload = $this->shouldReload($form_state);

      $reason = '';
      $result = $this->outgoing_mail_logger->resend($log, $reason);

      if (!$result) {
        $message_type = 'error';
        if ($reason) {
          $message = t("This message could not be resent: {$reason}.");
        } else {
          $message = t("Failed to resend message.  Please check the logs or contact an administrator.");
        }
      } else {
        $message = t("Message resent!@reload", ['@reload' => $should_reload ? '  Reloading results...' : '']);
        $message_type = 'status';
      }

      // Hide modal command
      $response->addCommand(new CloseModalDialogCommand());

      // Display success/failure message command
      $response->addCommand(new MessageCommand($message,NULL, ['type' => $message_type]));

      // Reload page
      if ($result && $should_reload) {
        $response->addCommand(new RedirectCommand(Url::fromRoute('outgoing_mail_logger.list')->toString()));
      }

    } catch (\Exception $e) {
      watchdog_exception('OML', $e);
      $response->addCommand(new MessageCommand(t('There was an error processing your request.  Please check the logs or contact an administrator.'),NULL, ['type' => 'error']));
    }

    return $response;
  }

  public function ajaxCancel(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

}
