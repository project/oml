<?php

namespace Drupal\outgoing_mail_logger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\outgoing_mail_logger\OutgoingMailLogger;

/**
 * Configure outgoing_mail_logger settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const SETTING_ENABLED = 'enabled';
  const SETTING_LOG_BODY = 'log_body';
  const SETTING_COMPRESS_MESSAGE = 'compress_message';
  const SETTING_MESSAGE_TYPES = 'message_types';
  const SETTING_MESSAGE_MODULES = 'message_modules';
  const SETTING_DATE_FORMAT = 'date_format';
  const SETTING_MAX_AGE = 'max_age';
  const SETTING_RESULTS_PER_PAGE = 'per_page';
  const SETTING_RESEND = 'resend';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'outgoing_mail_logger_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['outgoing_mail_logger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('outgoing_mail_logger.settings');
    $form[self::SETTING_ENABLED] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Logging'),
      '#default_value' => $config->get(self::SETTING_ENABLED),
    ];

    $form[self::SETTING_RESEND] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Resend'),
      '#default_value' => $config->get(self::SETTING_RESEND),
      '#description' => t('If enabled, messages with body text may be resent by authorized users.  In addition to this global configuration setting, a user must be assigned to a role with the OML rend permission enabled.  If a message was logged while the \'Log the body of the email\' option was disabled the logged message body will be empty and thus can not be resent.')
    ];

    $form[self::SETTING_LOG_BODY] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log the body of the email'),
      '#default_value' => $config->get(self::SETTING_LOG_BODY),
      '#description' => t('If not checked the body of the email will not be stored.  <strong>THIS SETTING IS NOT RETROACTIVE.</strong>'),
    ];

    $form[self::SETTING_COMPRESS_MESSAGE] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compress Message Body (<a href="https://www.php.net/manual/en/function.gzcompress.php" target="_blank">gzcompress</a>)'),
      '#default_value' => $config->get(self::SETTING_COMPRESS_MESSAGE),
      '#description' => t('If checked, the body of the email will be stored ina compressed format which reduce disk space usage.  If \'Log the body of the email\' is disabled, this setting does nothing.'),
    ];

    $form[self::SETTING_DATE_FORMAT] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sent Date Format'),
      '#description' => t('Enter date format to display sent date.  Please refer to <a href="https://www.php.net/manual/en/datetime.format.php" target="_blank">PHP date-time format documentation</a> for additional formats options. Default: ' . OutgoingMailLogger::DEFAULT_DATE_FORMAT),
      '#default_value' => $config->get(self::SETTING_DATE_FORMAT),
      '#placeholder' => OutgoingMailLogger::DEFAULT_DATE_FORMAT,
    ];

    $form[self::SETTING_MAX_AGE] = [
      '#type' => 'number',
      '#title' => $this->t('Purge Threshold'),
      '#description' => t('The maximum number of days to keep emails.  If set to zero, logs are never purged.'),
      '#field_suffix' => t('days'),
      '#step' => 1,
      '#min' => 0,
      '#default_value' => $config->get(self::SETTING_MAX_AGE) ?: 0,
    ];

    $form[self::SETTING_RESULTS_PER_PAGE] = [
      '#type' => 'number',
      '#title' => $this->t('Results per page'),
      '#description' => t('The number of results to display per page on the <a href="' . Url::fromRoute('outgoing_mail_logger.list')->toString() . '">logs report</a>.  Default: ' . OutgoingMailLogger::DEFAULT_RESULTS_PER_PAGE),
      '#default_value' => $config->get(self::SETTING_RESULTS_PER_PAGE) ?: OutgoingMailLogger::DEFAULT_RESULTS_PER_PAGE,
      '#step' => 5,
      '#min' => 5,
    ];

    $form[self::SETTING_MESSAGE_TYPES] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Types Filter (module-key)'),
      '#description' => t('Enter one message-type (module-key) per line to log specific message types.  *** FORMAT: <module-name>_<key> *** If empty, ALL message-types will be logged, unless they match a module filter.'),
      '#default_value' => $config->get(self::SETTING_MESSAGE_TYPES),
    ];

    $form[self::SETTING_MESSAGE_MODULES] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message Modules Filter'),
      '#description' => t('Enter one module (machine-name) per line to log messages for specific module.  If empty, ALL module messages will be logged, unless the match a message-type filter.'),
      '#default_value' => $config->get(self::SETTING_MESSAGE_MODULES),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('outgoing_mail_logger.settings');
    foreach ([
      self::SETTING_ENABLED,
      self::SETTING_LOG_BODY,
      self::SETTING_COMPRESS_MESSAGE,
      self::SETTING_MESSAGE_TYPES,
      self::SETTING_MESSAGE_MODULES,
      self::SETTING_DATE_FORMAT,
      self::SETTING_MAX_AGE,
      self::SETTING_RESULTS_PER_PAGE,
      self::SETTING_RESEND,
             ] as $setting) {
      $settings->set($setting, $form_state->getValue($setting));
    }
    $settings->save();
    parent::submitForm($form, $form_state);
  }

}
