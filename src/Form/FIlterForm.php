<?php

namespace Drupal\outgoing_mail_logger\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\outgoing_mail_logger\Model\OutgoingMailLog;
use Drupal\outgoing_mail_logger\OutgoingMailLogger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FIlterForm
 *
 * @package Drupal\outgoing_mail_logger\Form
 */
class FIlterForm extends FormBase {

  /** @var Connection */
  protected $connection;

  /** @var OutgoingMailLogger */
  protected $outgoing_mail_logger;

  /** @var EmailValidatorInterface  */
  protected $email_validator;

  /** @var bool */
  protected $purge = FALSE;

  /**
   * OutgoingMailLoggerController constructor.
   *
   * @param Connection $database
   * @param OutgoingMailLogger $outgoing_mail_logger
   * @param EmailValidatorInterface $email_validator
   */
  public function __construct(
    Connection $database,
    OutgoingMailLogger $outgoing_mail_logger,
    EmailValidatorInterface $email_validator
  ) {
    $this->connection = $database;
    $this->outgoing_mail_logger = $outgoing_mail_logger;
    $this->email_validator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('outgoing_mail_logger.service'),
      $container->get('email.validator'),
    );
  }

  /**
   * @inheritDoc
   */
  public function getFormId()
  {
    return 'oml_filter_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $filters = $this->processFilters($form_state);

    $date_range_from =
    $date_range_to =
    $key =
    $subject =
    $to = NULL;

    if (!empty($filters['date_range']['from'])) {
      $date_range_from = DrupalDateTime::createFromTimestamp($filters['date_range']['from']);
    }

    if (!empty($filters['date_range']['to'])) {
      $date_range_to = DrupalDateTime::createFromTimestamp($filters['date_range']['to']);
    }

    foreach (['key', 'subject', 'to'] as $filter) {
      if (!empty($filters[$filter])) {
        $$filter = $filters[$filter];
      }
    }

    if ($this->purge) {
      return [
        '#title' => t('Purge Mail Logs?'),
        'warn' => [
          '#type' => 'container',
          'content' => [
            '#markup' => '<strong>' . t('This action cannot be undone!  Are you sure?') . '</strong>'
          ],
        ],
        'actions' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-actions'],
          ],
          'purge_confirmed' => [
            '#type' => 'submit',
            '#value' => t('Confirm'),
          ],
          'cancel' => [
            '#type' => 'submit',
            '#value' => t('Cancel'),
          ],
        ],
      ];
    }

    $filters_are_empty = strcmp(json_encode($filters), json_encode($this->getDefaultFilters()));

    $form = [
      'filters' => [
        '#type' => 'details',
        '#title' => t('Filters'),
        '#collapsed' => empty($filters_are_empty),
        '#weight' => 10,
        'date_range' => [
          '#type' => 'container',
          'date_range_from' => [
            '#type' => 'datetime',
            '#title_display' => 'invisible',
            '#title' => $this->t('From'),
            '#default_value' => $date_range_from,
            '#date_time_element' => 'none',
          ],
          'date_range_to' => [
            '#type' => 'datetime',
            '#title_display' => 'invisible',
            '#title' => $this->t('To'),
            '#default_value' => $date_range_to,
            '#date_time_element' => 'none',
          ],
        ],
        'key' => [
          '#type' => 'textfield',
          '#title' => t('Key'),
          '#default_value' => $key,
        ],
        'subject' => [
          '#type' => 'textfield',
          '#title' => t('Subject'),
          '#default_value' => $subject,
        ],
        'to' => [
          '#type' => 'email',
          '#title' => t('To Address'),
          '#default_value' => $to,
        ],
        'actions' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-actions'],
          ],
          'submit' => [
            '#type' => 'submit',
            '#value' => t('Apply'),
          ],
          'reset' => [
            '#type' => 'submit',
            '#value' => t('Reset'),
          ],
        ],
      ],
      'results_table' => $this->getLogsTable($filters),
    ];

    if ($this->outgoing_mail_logger->getLogCount() > 0 && $this->currentUser()->hasPermission('oml can purge logs')) {
      $form['purge'] = [
        '#type' => 'details',
        '#title' => t('Purge Logs'),
        '#collapsed' => TRUE,
        '#weight' => 20,
        'actions' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-actions'],
          ],
          'purge_request' => [
            '#type' => 'submit',
            '#value' => t('Clear Logs'),
          ],
        ],
      ];
    }

    return $form;

  }

  /**
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $triggering_element_id = $form_state->getTriggeringElement()['#id'];
    if ($triggering_element_id === 'edit-purge-request') {
      $this->purge = TRUE;
      $form_state->setRebuild(TRUE);
    } else if ($triggering_element_id === 'edit-purge-confirmed') {
      if ($this->outgoing_mail_logger->purgeLogs()) {
        $this->messenger()->addMessage(t('Mail logs purged.'));
      } else {
        $this->messenger()->addMessage(t('Failed to purge mail logs.  Please check the logs or contact an administrator.'));
      }
    } else if ($triggering_element_id !== 'edit-reset') {
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * @inheritDoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);

    $fsv = $form_state->getValues();

    if (!$fsv) {
      return;
    }

    // Verify to is after from
    if (!empty($fsv['date_range_from']) && !empty($fsv['date_range_to'])) {
      if ($fsv['date_range_to']->getTimestamp() < $fsv['date_range_from']->getTimestamp()) {
        $form_state->setErrorByName('date_range_to', 'Date-range: TO date must be after selected FROM date');
      }
    }

    if (!empty($fsv['to'])) {
      if (!$this->email_validator->isValid($fsv['to'])) {
        $form_state->setErrorByName('to', "Invalid TO email parameter");
      }
    }
  }


  /**
   * Returns default filters array
   *
   * @return array
   */
  protected function getDefaultFilters()
  {
    return [
      'date_range' => ['from' => NULL, 'to' => NULL],
      'key' => NULL,
      'subject' => NULL,
      'to' => NULL,
    ];
  }

  /**
   * Generates renderable table
   *
   * @param array $filters
   * @param int $per_page
   *
   * @return array
   */
  public function getLogsTable(array $filters = [])
  {
    $query = $this->connection->select(OutgoingMailLog::TABLE, 'oml')
      ->fields('oml');

    $this->processQueryFilters($query, $filters);

    $query->orderBy('sent', 'DESC');

    $pager = $query->extend(PagerSelectExtender::class)
      ->limit($this->outgoing_mail_logger->getConfigResultsPerPage());
    $results = $pager->execute()->fetchAll();

    $header = array(
      'id' => $this->t('ID'),
      'sent' => $this->t('Date'),
      'key' => $this->t('Module-Key'),
      'to' => $this->t('To'),
      'subject' => $this->t('Subject'),
      'status' => $this->t('Status'),
      'actions' => $this->t('Actions'),
    );

    $global_resend_enabled = $this->outgoing_mail_logger->getConfigResend();

    $rows = [];
    foreach ($results as $result) {

      $log = new OutgoingMailLog($result);

      $details_link = Link::createFromRoute(t('Details'), 'outgoing_mail_logger.detail', ['id' => $log->getId()]);
      if ($global_resend_enabled) {
        $resend_link = $this->outgoing_mail_logger->getResendLink($log, TRUE);
      }

      $rows[] = [
        'id' => $log->getId(),
        'sent' => $log->getSent($this->outgoing_mail_logger->getConfigDateFormat()),
        'key' => $log->getKey(),
        'to' => $log->getTo(),
        'subject' => $log->getSubject(),
        'status' => (($status = $log->getStatus()) === NULL) ? '--' : (($status == 1) ? t('OK') : t('Failed')),
        'actions' => ['data' => [
            '#markup' => $details_link->toString() . (isset($resend_link) ? '<br>' . $resend_link->toString() : '')
          ]
        ],
      ];
    }

    // Generate the table.
    $build = [
      '#weight' => 50,
      'table' => [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ],
      'pager' => [
        '#type' => 'pager'
      ]
    ];

    if ($this->currentUser()->hasPermission('administer outgoing_mail_logger configuration')) {
      $link = Link::createFromRoute(t('Outgoing Mail Logger Settings'), 'outgoing_mail_logger.settings_form');
      $build['table']['#caption'] = $link->toRenderable();
    }

    if ($global_resend_enabled) {
      $build['#attached']['library'] = ['core/drupal.dialog.ajax'];
    }

    return $build;
  }

  /**
   * Process filter-form submission
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  protected function processFilters(FormStateInterface $form_state) : array
  {

    $filters = $this->getDefaultFilters();

    $fsv = $form_state->getValues();

    if(!$fsv) {
      return $filters;
    }

    if (!empty($fsv['date_range_from'])) {
      try {
        /** @var DrupalDateTime $from */
        $from = $fsv['date_range_from'];
        if (!($errors = $from->getErrors())) {
          $from->setTime(0, 0, 0);
          $filters['date_range']['from'] = $from->getTimestamp();
        } else {
          $this->messenger()->addError(implode(PHP_EOL, $errors));
        }
      } catch (\Exception $e) {
        $this->messenger()->addError("Invalid date parameter");
      }
    }

    if (!empty($fsv['date_range_to'])) {
      try {
        /** @var DrupalDateTime $to */
        $to = $fsv['date_range_to'];
        if (!($errors = $to->getErrors())) {
          $to->setTime(23, 59, 59);
          $filters['date_range']['to'] = $to->getTimestamp();
        } else {
          $this->messenger()->addError(implode(PHP_EOL, $errors));
        }
      } catch (\Exception $e) {
        $this->messenger()->addError("Invalid date parameter");
      }
    }

    if (!empty($fsv['key'])) {
      $filters['key'] = Html::escape($fsv['key']);
    }

    if (!empty($fsv['subject'])) {
      $filters['subject'] = Html::escape($fsv['subject']);
    }

    if (!empty($fsv['to'])) {
      $filters['to'] = $fsv['to'];
    }

    return $filters;

  }

  /**
   * Adds query conditions based on defined filters
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   * @param array $filters
   */
  protected function processQueryFilters(SelectInterface $query, array $filters)
  {
    if (!empty($filters['date_range']['from']) && !empty($filters['date_range']['to'])) {
      $query->condition('sent', [$filters['date_range']['from'], $filters['date_range']['to']], 'BETWEEN');
    } else if (!empty($filters['date_range']['from'])) {
      $query->condition('sent', $filters['date_range']['from'], '>');
    } else if (!empty($filters['date_range']['to'])) {
      $query->condition('sent', $filters['date_range']['to'], '<');
    }

    foreach (['key', 'subject', 'to'] as $filter) {
      if (!empty($filters[$filter])) {
        $query->condition($filter, "%{$filters[$filter]}%", 'LIKE');
      }
    }
  }

  /**
   * This only applies to purge operation.
   *
   * @inheritDoc
   */
  public function getQuestion()
  {
    return t('This action can not be undone.');
  }

  /**
   * This only applies to purge operation.
   *
   * @inheritDoc
   */
  public function getCancelUrl()
  {
    return Url::fromRoute('outgoing_mail_logger.list')->toString();
  }

}
