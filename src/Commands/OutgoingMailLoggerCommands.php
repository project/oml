<?php

namespace Drupal\outgoing_mail_logger\Commands;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Class OutgoingMailLoggerCommands
 *
 * @package outgoing_mail_logger
 */
class OutgoingMailLoggerCommands  extends DrushCommands {

  /** @var MailManagerInterface $mail_manager */
  protected $mail_manager;

  /** @var EmailValidatorInterface  */
  protected $email_validator;

  /**
   * OutgoingMailLoggerCommands constructor.
   *
   * @param MailManagerInterface $mail_manager
   * @param EmailValidatorInterface $email_validator
   */
  public function __construct(MailManagerInterface $mail_manager, EmailValidatorInterface $email_validator) {
    parent::__construct();
    $this->mail_manager = $mail_manager;
    $this->email_validator = $email_validator;
  }

  /**
   * Send test email
   *
   * @command outgoing-mail-logger:send-mail
   * @aliases oml:send
   *
   * @usage
   *    outgoing-mail-logger:send-mail --to=example@example.com --subject="OML Test" --body="This is a test."
   *
   * @param array $options
   */
  public function sendMail(array $options = [
    'to' => 'example@example.com',
    'subject' => self::OPT,
    'body' => self::OPT,
  ]) {

    try {

      if (!$this->email_validator->isValid($options['to'])) {
        throw new \InvalidArgumentException("Invalid email argument.");
      }

      $params = [];
      if (!empty($options['body'])) {
        $params['body'] = (array) $options['body'];
      }

      if (!empty($options['subject'])) {
        $params['subject'] = $options['subject'];
      }

      $this->io()->text(
        PHP_EOL . 'Sending test email...' . PHP_EOL
        . "\tTo: {$options['to']}" . PHP_EOL
        . ($params['subject'] ? "\tSubject: {$params['subject']}" . PHP_EOL : '')
        . ($params['body'] ?  "\tBody: " . implode(PHP_EOL, $params['body']) . PHP_EOL : '')
      );

      $send = $this->mail_manager->mail(
        'outgoing_mail_logger',
        'oml-test',
        $options['to'],
        LanguageInterface::LANGCODE_NOT_SPECIFIED,
        $params,
        NULL,
        TRUE
      );

      $this->io()->text(($send['result'] ? 'Message sent.' : 'Message failed.'));

    } catch (\Exception $e) {
      watchdog_exception('OML', $e);
      $this->io()->error('Exception: ' . $e->getMessage());
    }

  }

}
