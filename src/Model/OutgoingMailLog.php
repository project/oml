<?php
/**
 * Project: hammer9-rebuild
 *
 * @copyright 2021 Mosaic Learning Inc.
 */

namespace Drupal\outgoing_mail_logger\Model;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Language\LanguageInterface;

/**
 * Class OutgoingMailLog
 *
 * @package Drupal\outgoing_mail_logger\Model
 */
class OutgoingMailLog {

  const TABLE = 'outgoing_mail_logger';

  /**
   * @var int
   */
  public $id;

  /**
   * @var int
   */
  public $uid;

  /**
   * @var string
   */
  public $key;

  /**
   * @var string
   */
  public $to;

  /**
   * @var string
   */
  protected $from;

  /**
   * @var string
   */
  protected $subject;

  /**
   * @var array
   */
  protected $body;

  /**
   * @var array
   */
  protected $headers;

  /**
   * @var int
   */
  protected $sent;

  /**
   * @var string
   */
  protected $ipaddress;

  /**
   * @var string
   */
  protected $language;

  /**
   * @var string
   */
  protected $mail_system;

  /**
   * @var int|null
   */
  protected $status;

  /**
   * OutgoingMailLog constructor.
   *
   * @param \StdClass $record
   */
  public function __construct(\StdClass $record) {
    foreach (get_object_vars($record) as $property => $value) {
      $setter = 'set' . lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $property))));
      if (method_exists($this, $setter)) {
        $this->$setter($value);
      } else {
        throw new \RuntimeException("Invalid property: {$property}");
      }
    }
  }

  /**
   * @return int|null
   */
  public function getId() : ?int
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId(int $id) : void
  {
    if (!isset($this->id)) {
      $this->id = $id;
    } else {
      throw new \RuntimeException("This model instance id is already defined.");
    }
  }

  /**
   * @return int
   */
  public function getUid() : int
  {
    return $this->uid;
  }

  /**
   * @param int $uid
   */
  protected function setUid(int $uid) : void
  {
    $this->uid = $uid;
  }

  /**
   * @return string
   */
  public function getKey() : string
  {
    return $this->key;
  }

  /**
   * @param string $key
   */
  protected function setKey(string $key) : void
  {
    $this->key = $key;
  }

  /**
   * @return string
   */
  public function getTo() : string
  {
    return $this->to;
  }

  /**
   * @param string $to
   */
  protected function setTo(string $to) : void
  {
    $this->to = $to;
  }

  /**
   * @return string
   */
  public function getFrom() : string
  {
    return $this->from;
  }

  /**
   * @param string $from
   */
  protected function setFrom(string $from) : void
  {
    $this->from = $from;
  }

  /**
   * @return string
   */
  public function getSubject() : string
  {
    return $this->subject;
  }

  /**
   * @param string $subject
   */
  protected function setSubject(string $subject) : void
  {
    $this->subject = $subject;
  }

  /**
   * @return array
   */
  public function getBody() : array
  {
    return $this->body;
  }

  /**
   * @param string|array $body
   */
  protected function setBody($body) : void
  {
    $this->body = (array) $body;
  }

  /**
   * @return array
   */
  public function getHeaders() : array
  {
    return $this->headers;
  }

  /**
   * @param array|string $headers
   */
  protected function setHeaders($headers) : void
  {
    if (is_string($headers) && !empty($headers)) {
      $headers = json_decode($headers, TRUE);
    }
    $this->headers = $headers;
  }

  /**
   * @param string|null $format
   *
   * @return int|DrupalDateTime|string
   */
  public function getSent(string $format = NULL)
  {
    if (!empty($format)) {
      $tz = \Drupal::configFactory()->get('system.date')->get('timezone.default');
      $date = DrupalDateTime::createFromTimestamp($this->sent, $tz);
      if ($format === 'date_time') {
        return $date;
      } else {
        return $date->format($format);
      }
    }
    return $this->sent;
  }

  /**
   * @param int $sent
   */
  protected function setSent(int $sent) : void
  {
    $this->sent = $sent;
  }

  /**
   * @return string
   */
  public function getIpaddress() : string
  {
    return $this->ipaddress;
  }

  /**
   * @param string|null $ipaddress
   */
  protected function setIpaddress(?string $ipaddress) : void
  {
    $this->ipaddress = $ipaddress;
  }

  /**
   * @return string
   */
  public function getLanguage() : string
  {
    return $this->language;
  }

  /**
   * @param string $language
   */
  protected function setLanguage(string $language) : void
  {
    $this->language = $language;
  }

  /**
   * @return string
   */
  public function getMailSystem() : string
  {
    return $this->mail_system;
  }

  /**
   * @param string|array $mail_system
   */
  protected function setMailSystem($mail_system) : void
  {
    $this->mail_system = (array) $mail_system;
  }

  /**
   * @return int|null
   */
  public function getStatus() : ?int
  {
    return $this->status;
  }

  /**
   * @param int|null $status
   */
  public function setStatus(?int $status) : void
  {
    $this->status = $status;
  }

  /**
   * @return array
   */
  public function toArray() : array
  {

    $array = [];

    foreach (get_object_vars($this) as $property => $value) {
      $array[$property] = $value;
    }

    $array['headers'] = json_encode($array['headers']);

    if (is_array($array['mail_system'])) {
      $array['mail_system'] = json_encode($array['mail_system']);
    }

    if (empty($array['language'])) {
      $array['language'] = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    }

    $array['body'] = '';
    foreach ($this->body as $body_part) {
      $array['body'] .= PHP_EOL . (string) $body_part;
    }

    return $array;
  }
}
