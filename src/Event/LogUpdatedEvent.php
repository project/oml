<?php

namespace Drupal\outgoing_mail_logger\Event;

use Drupal\outgoing_mail_logger\Model\OutgoingMailLog;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class LogUpdatedEvent
 *
 * Event fired AFTER an OML log record is updated.
 *
 * @package Drupal\outgoing_mail_logger\Event
 */
class LogUpdatedEvent extends Event {

  /**
   * Log instance
   *
   * @var OutgoingMailLog $log
   */
  protected $log;

  /**
   * LogUpdatedEvent constructor.
   *
   * @param OutgoingMailLog $log
   */
  public function __construct(OutgoingMailLog $log) {
    $this->log = $log;
  }

  /**
   * @return OutgoingMailLog
   */
  public function getLog() : OutgoingMailLog
  {
    return $this->log;
  }

}
