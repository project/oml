<?php

namespace Drupal\outgoing_mail_logger\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class MailSentEvent
 *
 * Event fired mail is processed by the outgoing_mail_logger :: hook_mail implementation.
 *
 * @package Drupal\outgoing_mail_logger\Event
 */
class MailSentEvent extends Event {

  /**
   * Mail message array
   *
   * @var array &$message
   */
  public $message;

  /**
   * MailSentEvent constructor.
   *
   * @param string $key
   * @param array $message
   */
  public function __construct(array &$message) {
    $this->message =& $message;
  }

  /**
   * @return array
   */
  public function getMessage() : array
  {
    return $this->message;
  }

}
