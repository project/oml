<?php

namespace Drupal\outgoing_mail_logger\Event;

/**
 * Class OutgoingMailLoggerEvents
 *
 * Defines events for the outgoing_mail_logger events submodule
 *
 * @package Drupal\outgoing_mail_logger\Event
 */
final class OutgoingMailLoggerEvents {

  /**
   * Name of the event fired when mail is processed by the outgoing_mail_logger :: hook_mail implementation.
   *
   * @Event
   *
   * @see \Drupal\outgoing_mail_logger\Event\MailSentEvent
   */
  const OML_EVENT_SENT = 'outgoing_mail_logger.event.mail_sent';

  /**
   * Name of the event fired when a new OML log record is created.
   *
   * @Event
   *
   * @see \Drupal\outgoing_mail_logger\Event\LogCreatedEvent
   */
  const OML_EVENT_LOG_CREATED = 'outgoing_mail_logger.event.log_created';

  /**
   * Name of the event fired when a new OML log record is updated.
   *
   * @Event
   *
   * @see \Drupal\outgoing_mail_logger\Event\LogUpdatedEvent
   */
  const OML_EVENT_LOG_UPDATED = 'outgoing_mail_logger.event.log_updated';

}
