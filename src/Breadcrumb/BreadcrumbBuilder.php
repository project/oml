<?php

namespace Drupal\outgoing_mail_logger\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\Entity\User;

/**
 * Class BreadcrumbBuilder
 *
 * @package Drupal\hammer9\Breadcrumb
 */
class BreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /** @var \Drupal\Core\Session\AccountProxy $current_user */
  protected $current_user;

  /**
   * BreadcrumbBuilder constructor.
   *
   * @param AccountProxy $current_user
   */
  public function __construct(AccountProxy $current_user) {
    $this->current_user = User::load($current_user->id());
  }

  /**
   * @inheritDoc
   */
  public function applies(RouteMatchInterface $route_match)
  {

    $parameters = $route_match->getParameters()->all();

    $watch_keys = [];

    $route_name = $route_match->getRouteName();

    $watch_routes = [
      'outgoing_mail_logger.list',
      'outgoing_mail_logger.detail',
    ];

    return array_intersect($watch_keys, array_keys($parameters)) || in_array($route_name, $watch_routes);

  }

  /**
   * @inheritDoc
   */
  public function build(RouteMatchInterface $route_match)
  {
    // Define a new object of type Breadcrumb
    $breadcrumb = new Breadcrumb();

    /** @var \Drupal\hammer9_events\Entity\RegistrationEntity $registration */
    /** @var \Drupal\hammer9_events\Entity\EventEntity $event */

    $route_name = $route_match->getRouteName();

    $breadcrumb->addLink(Link::createFromRoute('Home', '<front>'));
    $breadcrumb->addLink(Link::createFromRoute('Administration', 'system.admin'));
    $breadcrumb->addLink(Link::createFromRoute('Reports', 'system.admin_reports'));
    switch ($route_name) {
      case 'outgoing_mail_logger.list':
        $breadcrumb->addLink(Link::createFromRoute('Outgoing Mail Logs', '<nolink>'));
        break;
      case 'outgoing_mail_logger.detail':
        $breadcrumb->addLink(Link::createFromRoute('Outgoing Mail Logs', 'outgoing_mail_logger.list'));
        break;
    }

    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;

  }

}
