<?php

namespace Drupal\outgoing_mail_logger;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Link;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\outgoing_mail_logger\Event\LogCreatedEvent;
use Drupal\outgoing_mail_logger\Event\LogUpdatedEvent;
use Drupal\outgoing_mail_logger\Event\OutgoingMailLoggerEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\outgoing_mail_logger\Form\SettingsForm;
use Drupal\outgoing_mail_logger\Model\OutgoingMailLog;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\AccountProxy;

class OutgoingMailLogger {

  const DEFAULT_DATE_FORMAT = 'Y-m-d g:i A';
  const DEFAULT_RESULTS_PER_PAGE = 25;

  /** @var Connection */
  protected $connection;

  /** @var ConfigFactoryInterface */
  protected $oml_settings;

  /** @var FileSystemInterface  */
  protected $file_system;

  /** @var EventDispatcherInterface  */
  protected $event_dispatcher;

  /** @var AccountProxy */
  protected $current_user;

  /** @var MailManagerInterface  */
  protected $mail_manager;

  /**
   * OutgoingMailLogger constructor.
   *
   * @param Connection $database
   * @param ConfigFactoryInterface $config_factory
   * @param FileSystemInterface $file_system
   * @param EventDispatcherInterface $event_dispatcher
   */
  public function __construct(
    Connection $database,
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    EventDispatcherInterface $event_dispatcher,
    AccountProxy $current_user,
    MailManagerInterface $mail_manager
  ) {
    $this->connection = $database;
    $this->oml_settings = $config_factory->get('outgoing_mail_logger.settings');
    $this->file_system = $file_system;
    $this->event_dispatcher = $event_dispatcher;
    $this->current_user = $current_user;
    $this->mail_manager = $mail_manager;
  }

  /**
   * Returns TRUE if logging is enabled
   *
   * @return bool
   */
  public function getConfigIsEnabled() : bool
  {
    return (bool) $this->oml_settings->get(SettingsForm::SETTING_ENABLED);
  }

  /**
   * Returns loggable message-type filter values
   *
   * @return array
   */
  public function getConfigMessageTypeFilterValues() : array
  {
    $types = [];
    if ($keys = $this->oml_settings->get(SettingsForm::SETTING_MESSAGE_TYPES)) {
      $types = array_map('trim', explode(PHP_EOL, $keys));
    }
    return $types;
  }

  /**
   * Returns loggable message-module filter values
   *
   * @return array
   */
  public function getConfigMessageModuleFilterValues() : array
  {
    $types = [];
    if ($keys = $this->oml_settings->get(SettingsForm::SETTING_MESSAGE_MODULES)) {
      $types = array_map('trim', explode(PHP_EOL, $keys));
    }
    return $types;
  }

  /**
   * Returns display date format
   *
   * @return string
   */
  public function getConfigDateFormat() : string
  {
    return $this->oml_settings->get(SettingsForm::SETTING_DATE_FORMAT) ?: self::DEFAULT_DATE_FORMAT;
  }

  /**
   * Returns the maximum number of days to keep the log.
   *
   * @return int
   */
  public function getConfigMaxAge() : int
  {
    return $this->oml_settings->get(SettingsForm::SETTING_MAX_AGE) ?: 0;
  }

  /**
   * Returns the maximum number of results to display per page on the logs report.
   *
   * @return int
   */
  public function getConfigResultsPerPage() : int
  {
    return $this->oml_settings->get(SettingsForm::SETTING_RESULTS_PER_PAGE) ?: self::DEFAULT_RESULTS_PER_PAGE;
  }

  /**
   * Returns global resend configuration value.
   *
   * @return bool
   */
  public function getConfigResend() : bool
  {
    return $this->oml_settings->get(SettingsForm::SETTING_RESEND) ?: FALSE;
  }

  /**
   * Returns TRUE if configured  to store email body.
   *
   * @return bool
   */
  public function getConfigShouldSaveBody() : bool
  {
    return $this->oml_settings->get(SettingsForm::SETTING_LOG_BODY) ?: FALSE;
  }

  /**
   * Returns TRUE if the message body should be compressed before storage.
   *
   * @return bool
   */
  public function getConfigShouldCompressMessage() : bool
  {
    return (bool) $this->oml_settings->get(SettingsForm::SETTING_COMPRESS_MESSAGE);
  }

  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////
  // SERVICE OPERATIONS
  //////////////////////////////////////////////////////
  //////////////////////////////////////////////////////

  /**
   * Returns TRUE if the OML log can be sent based on global resend setting, user access, and log properties.
   *
   * @param \Drupal\outgoing_mail_logger\Model\OutgoingMailLog $log
   * @param string $reason
   *
   * @return bool
   */
  public function canResend(OutgoingMailLog $log, string &$reason = '') : bool
  {

    if (!$this->getConfigResend()) {
      $reason = 'global resend option is disabled';
      return FALSE;
    }

    if (!$this->current_user->hasPermission('oml can resend')) {
      $reason = 'this use is not permitted to resend';
      return FALSE;
    }

    if (empty($log->getBody())) {
      $reason = 'log has no body';
      return FALSE;
    }

    return TRUE;

  }

  /**
   * Returns TRUE if the message type should be saved.
   * Default: TRUE
   *
   * @param array $message
   *
   * @return bool
   */
  public function canSave(array $message) : bool
  {
    $valid = TRUE;

    if ($loggable_modules = $this->getConfigMessageModuleFilterValues()) {
      $valid = in_array($message['module'], $loggable_modules);
    }

    if ($loggable_types = $this->getConfigMessageTypeFilterValues()) {
      $valid = in_array("{$message['module']}_{$message['key']}", $loggable_types);
    }

    return $valid;
  }

  /**
   * @param int $id
   *
   * @return \Drupal\outgoing_mail_logger\Model\OutgoingMailLog|null
   */
  public function getLogById(int $id) : ?OutgoingMailLog
  {
    $record = $this->connection->select(OutgoingMailLog::TABLE, 'oml')
      ->fields('oml')
      ->condition('id', $id)
      ->execute()
      ->fetchObject();

    if ($record) {

      if (!$validUTF8 = mb_check_encoding($record->body, 'UTF-8')) {
        $data = (array) $record;
        try {
          $this->decompressMessage($data);
        } catch (\Exception $e) {
          watchdog_exception('OML', $e);
          $data['body'] = "Message decompression failed.";
        }
        $record = (object) $data;
      }

      return new OutgoingMailLog($record);
    }
  }

  /**
   * Returns the total number of logs
   *
   * @return int
   */
  public function getLogCount() : int
  {
    return (int) $this->connection->select(OutgoingMailLog::TABLE)
      ->countQuery()
      ->execute()
      ->fetchField();
  }

  /**
   * Returns resend message link object
   *
   * @param OutgoingMailLog $log
   * @param bool $reload
   *
   * @return Link|null
   */
  public function getResendLink(OutgoingMailLog $log, bool $reload = FALSE) : ?Link
  {

    if (!$this->canResend($log)) {
      return NULL;
    }

    $url = Url::fromRoute('outgoing_mail_logger.ajax.resend', [
      'id' => $log->getId(),
    ]);

    $url->setOptions([
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 400]),
      ],
      'query' => ['reload' => $reload]
    ]);

    return Link::fromTextAndUrl(t('Resend'), $url);

  }

  /**
   * @param \Drupal\outgoing_mail_logger\Model\OutgoingMailLog $log
   *
   * @return \Drupal\Core\Database\StatementInterface|int|string|null
   */
  public function saveLog(OutgoingMailLog $log)
  {

    $result = FALSE;

    try {

      $data = $log->toArray();

      if ($this->getConfigShouldSaveBody()) {
        if ($this->getConfigShouldCompressMessage()) {
          $this->compressMessage($data);
        }
      } else {
        unset($data['body']);
      }

      if (empty($log->getId())) {
        $result = $this->connection
          ->insert(OutgoingMailLog::TABLE)
          ->fields($data)
          ->execute();
        $log->setId($result);
        $this->event_dispatcher->dispatch(OutgoingMailLoggerEvents::OML_EVENT_LOG_CREATED, new LogCreatedEvent($log));
      }
      else {
        unset($data['id']);
        $result = $this->connection
          ->update(OutgoingMailLog::TABLE)
          ->condition('id', $log->getId())
          ->fields($data)
          ->execute();
        $this->event_dispatcher->dispatch(OutgoingMailLoggerEvents::OML_EVENT_LOG_UPDATED, new LogUpdatedEvent($log));
      }

    } catch (\Exception $e) {
      watchdog_exception('OML', $e);
    }

    return $result;
  }

  /**
   * Resends logged message.
   *
   * @param OutgoingMailLog $log
   * @param string $reason
   *
   * @return bool
   */
  public function resend(OutgoingMailLog $log, string &$reason = '') : bool
  {
    try {

      if ($this->canResend($log, $reason)) {
        $params = [
          'from' => $log->getFrom(),
          'subject' => $log->getSubject(),
          'body' => $log->getBody(),
        ];
        $send = $this->mail_manager->mail('outgoing_mail_logger', 'resend', $log->getTo(), $log->getLanguage(), $params, NULL, TRUE);
      } else {
        throw new \RuntimeException("This message can not be resent ({$reason}).");
      }

    } catch (\Exception $e) {
      watchdog_exception('OML', $e);
    }

    return isset($send['result']) ? $send['result'] : FALSE;
  }

  /**
   * Processes an outgoing message for log data and creates a new (unsaved) log instance.
   *
   * @param array $message
   *
   * @return OutgoingMailLog
   */
  public function processMessage(array $message) : OutgoingMailLog
  {
    $mail_system = '';
    if ($mail_settings = \Drupal::config('mailsystem.settings')->get()) {
      $mail_system = $mail_settings['defaults'];
    }

    if (is_string($message['subject'])) {
      $subject = $message['subject'];
    } else if ($message['subject'] instanceof TranslatableMarkup) {
      $subject = $message['subject']->render();
    }

    $data = [
      'key' => "{$message['module']}_{$message['key']}",
      'to' => $message['to'],
      'from' => $message['from'],
      'subject' => $subject,
      'body' => $message['body'],
      'headers' => $message['headers'],
      'sent' => time(),
      'language' => $message['langcode'],
      'uid' => \Drupal::currentUser()->id(),
      'ipaddress' => $_SERVER['SERVER_ADDR'],
      'mail_system' => $mail_system ?: '',
    ];

    return new OutgoingMailLog((object) $data);

  }

  /**
   * Compresses message content
   *
   * @param array $data
   */
  protected function compressMessage(array &$data)
  {

    if (empty($data['body'])) {
      return;
    }

    $data['body'] = gzdeflate($data['body']);

  }

  /**
   * Decompresses message body
   *
   * @param array $data
   */
  protected function decompressMessage(array &$data)
  {
    if (empty($data['body'])) {
      return;
    }

    $data['body'] = gzinflate($data['body']);
  }

  /**
   * Removes expired logs
   */
  public function deleteExpired()
  {
    $settings = \Drupal::configFactory()->get('outgoing_mail_logger.settings');
    $log_maximum_age = $settings->get('max_age') ?: 0; // days
    if ($log_maximum_age > 0) {
      $log_maximum_age *= 86400;
      $this->connection->delete(OutgoingMailLog::TABLE)
        ->condition('sent', time() - $log_maximum_age, '<')
        ->execute();
    }
  }

  /**
   * @param OutgoingMailLog $log
   * @param bool $success
   */
  public function updateLogStatus(OutgoingMailLog $log, bool $success)
  {
    $log->setStatus($success);
    $this->saveLog($log);
  }

  /**
   * Purge all logs
   */
  public function purgeLogs()
  {
    if ($this->current_user->hasPermission('oml can purge logs')) {
      try {
        $this->connection->truncate(OutgoingMailLog::TABLE)->execute();
        return $this->getLogCount() === 0;
      } catch (\Exception $e) {
        watchdog_exception('OML', $e);
      }
    }
    return FALSE;
  }

}
