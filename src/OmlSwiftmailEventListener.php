<?php

namespace Drupal\outgoing_mail_logger;

use Swift_Events_SendEvent;

/**
 * Class OmlSwiftmailEventListener
 *
 * @package Drupal\outgoing_mail_logger
 */
class OmlSwiftmailEventListener implements \Swift_Events_SendListener {

  /**
   * Before email sent
   *
   * @param \Swift_Events_SendEvent $event
   */
  public function beforeSendPerformed(\Swift_Events_SendEvent $event)
  {

  }

  /**
   * @inheritDoc
   */
  public function sendPerformed(Swift_Events_SendEvent $evt)
  {
    try {
      if ($cache = drush_cache_get('oml-swiftmailer-tracking')) {
        $swiftmessage_id = $evt->getMessage()->getId();
        if (isset($cache->data[$swiftmessage_id])) {
          /** @var \Drupal\outgoing_mail_logger\OutgoingMailLogger $oml_service */
          $oml_service = \Drupal::service('outgoing_mail_logger.service');
          if ($log = $oml_service->getLogById($cache->data[$swiftmessage_id])) {
            $oml_service->updateLogStatus($log, $evt->getResult() === Swift_Events_SendEvent::RESULT_SUCCESS);
          }
        }
      }
    } catch (\Exception $e) {
      watchdog_exception('OML', $e);
    }
  }

}
