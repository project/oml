<?php

namespace Drupal\outgoing_mail_logger\EventSubscriber;

use Drupal\outgoing_mail_logger\Event\MailSentEvent;
use Drupal\outgoing_mail_logger\OutgoingMailLogger;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * outgoing_mail_logger event subscriber.
 *
 * @package Drupal\outgoing_mail_logger\EventSubscriber
 */
class OutgoingMailLoggerSubscriber implements EventSubscriberInterface {

  /** @var OutgoingMailLogger $outgoing_mail_logger */
  protected $outgoing_mail_logger;

  /**
   * OutgoingMailLoggerSubscriber constructor.
   *
   * @param OutgoingMailLogger $outgoing_mail_logger
   */
  public function __construct(OutgoingMailLogger $outgoing_mail_logger)
  {
    $this->outgoing_mail_logger = $outgoing_mail_logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    return [
      'outgoing_mail_logger.event.mail_sent' => ['handleMailSent', -30],
    ];
  }

  /**
   * @param MailSentEvent $mail_sent
   */
  public function handleMailSent(MailSentEvent $mail_sent)
  {
    if (!$this->outgoing_mail_logger->getConfigIsEnabled()) {
      return;
    }

    if (!$this->outgoing_mail_logger->canSave($mail_sent->message)) {
      return;
    }

    if ($log = $this->outgoing_mail_logger->processMessage($mail_sent->message)) {
      // Save log and append id to message reference
      $mail_sent->message['omlid'] = $this->outgoing_mail_logger->saveLog($log);
    }
  }

}
