<?php

namespace Drupal\outgoing_mail_logger\Tests;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\outgoing_mail_logger\Form\SettingsForm;
use Drupal\Tests\system\Kernel\Theme\FunctionsTest;
use Drupal\Tests\UnitTestCase;

/**
 * Tests site configuration.
 *
 * @group outgoing_mail_logger
 */
class OutgoingMailLoggerTest extends FunctionsTest {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'mailsystem',
    'outgoing_mail_logger',
  ];

  protected $oml_config_factory;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
//    $config_map = [
//      'outgoing_mail_logger.settings' => [
//         SettingsForm::SETTING_RESULTS_PER_PAGE => 10,
//         SettingsForm::SETTING_MESSAGE_MODULES => [],
//         SettingsForm::SETTING_MESSAGE_TYPES => [],
//         SettingsForm::SETTING_MAX_AGE => 10,
//         SettingsForm::SETTING_DATE_FORMAT => 'm/d/Y',
//         SettingsForm::SETTING_COMPRESS_MESSAGE => TRUE,
//         SettingsForm::SETTING_ENABLED => TRUE,
//      ]
//    ];
//
//    $this->oml_config_factory = $this->getConfigFactoryStub($config_map);
//
//    $container = new ContainerBuilder();
//
//    // Set the config.factory in the container also.
//    $container->set('config.factory', $this->oml_config_factory);
//
//    \Drupal::setContainer($container);
  }

  /**
   * Test site information form.
   */
  public function testFieldStorageSettingsForm() {
    /** @var \Drupal\outgoing_mail_logger\OutgoingMailLogger $oml_service */
    $oml_service = \Drupal::service('outgoing_mail_logger.service');
    $this->assertEquals(15, $oml_service->getConfigMaxAge());
  }

}
